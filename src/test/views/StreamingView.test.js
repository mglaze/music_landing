import React from "react";
import { shallow } from "enzyme";
import StreamingView from "../../components/views/streamingViewComponent";

import releaseTestData from "../../releases/never/data/neverRelease"

describe("StreamingView Component", () => {
  it("renders without crashing", () => {
    shallow(<StreamingView release={{ albumArt: [], music: { links: { streaming: [] } } }} />);
  });

  it("renders Header component for each album art", () => {
    const wrapper = shallow(<StreamingView release={releaseTestData} />);
    expect(wrapper.find("Header").length).toEqual(releaseTestData.albumArt.length);
  });

  it("renders StreamComponent for each streaming link", () => {
    const wrapper = shallow(<StreamingView release={releaseTestData} />);
    expect(wrapper.find("StreamComponent").length).toEqual(releaseTestData.music.links.streaming.length);
  });

  it("passes props to Header component correctly", () => {
    const wrapper = shallow(<StreamingView release={releaseTestData} />);
    releaseTestData.albumArt.forEach((art, index) => {
      const headerProps = wrapper.find("Header").at(index).props();
      expect(headerProps).toEqual(expect.objectContaining(art));
    });
  });

  it("passes props to StreamComponent correctly", () => {
    const wrapper = shallow(<StreamingView release={releaseTestData} />);
    releaseTestData.music.links.streaming.forEach((music, index) => {
      const streamComponentProps = wrapper.find("StreamComponent").at(index).props();
      expect(streamComponentProps).toEqual(expect.objectContaining(music));
    });
  });
});

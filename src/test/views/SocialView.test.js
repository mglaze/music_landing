import React from "react";
import { shallow } from "enzyme";
import SocialView from "../../components/views/socialViewComponent";

import releaseData from "../../releases/neter/data/neterRelease";
import socialIconsData from "../../assets/social/socialMediaIcons";
import donationIconsData from "../../assets/donation/donationIcons";

describe("SocialView Component", () => {
  it("renders without crashing", () => {
    shallow(<SocialView release={releaseData} />);
  });

  it("renders Header component", () => {
    const wrapper = shallow(<SocialView release={releaseData} />);
    expect(wrapper.find("Header").exists()).toBeTruthy();
  });

  it("renders Video component", () => {
    const wrapper = shallow(<SocialView release={releaseData} />);
    expect(wrapper.find("Video").exists()).toBeTruthy();
  });

  it("renders StreamComponent for each social link", () => {
    const wrapper = shallow(<SocialView release={releaseData} />);
    expect(wrapper.find("StreamComponent").length).toEqual(releaseData.music.links.social.length);
  });

  it("renders Social component for each social icon", () => {
    const wrapper = shallow(<SocialView release={releaseData} />);
    expect(wrapper.find("Social").length).toEqual(socialIconsData.icons.length);
  });

  it("renders Donation component for each donation icon", () => {
    const wrapper = shallow(<SocialView release={releaseData} />);
    expect(wrapper.find("Donation").length).toEqual(donationIconsData.icons.length);
  });
});

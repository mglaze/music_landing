import React from 'react';
import ReactDOM from 'react-dom';
import Icons from '../../components/Icons.js'

it('<Icons /> renders without crashing', () => {
    const div = document.createElement('div');
    const props = {
      music: {
        links: []
      }
    }
    ReactDOM.render(<Icons {...props} />, div);
    ReactDOM.unmountComponentAtNode(div);
  });
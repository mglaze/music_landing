import React from 'react';
import ReactDOM from 'react-dom';
import Video from '../../components/Video'

describe('<Video />', () => {
    const div = document.createElement('div');
    const unMount = (div) => { return ReactDOM.unmountComponentAtNode(div);}
    const props = {
        video: {
            youtube: ""
        }
    }
    it('<Video /> renders without crashing', () => {
        ReactDOM.render(<Video {... props} />, div);
        unMount(div);
    });

})



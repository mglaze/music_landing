import React from "react";
import { mount } from "enzyme";
import Donation from "../../components/donation/Donation";
import { analytics } from "../../utils/analyticsWrapper";

describe('Donation Component', () => {
  it('should render without crashing', () => {
    const wrapper = mount(
      <Donation
        id="1"
        tag="donation_clicked"
        url="/donate"
        iconImage="icon.png"
        name="Donation"
      />
    );
    expect(wrapper).toBeTruthy();
  });

  it('should call handleClick and change location on click', () => {
    const mockHandleClick = jest.fn();
    const mockAnalyticsEvent = jest.spyOn(analytics, 'event');
    const wrapper = mount(
      <Donation
        id="1"
        tag="donation_clicked"
        url="/donate"
        iconImage="icon.png"
        name="Donation"
      />
    );
    wrapper.instance().handleClick = mockHandleClick;
    wrapper.update();
    wrapper.find('.btn-img').simulate('click');
    expect(mockHandleClick).toHaveBeenCalledWith('donation_clicked', '');
    console.log('mockAnalyticsEvent calls:', mockAnalyticsEvent.mock.calls);
  });

  it('should render icon image with correct props', () => {
    const wrapper = mount(
      <Donation
        id="1"
        tag="donation"
        url="/donate"
        iconImage="icon.png"
        name="Donation"
      />
    );
    const imgElement = wrapper.find('.btn-img');
    expect(imgElement.prop('src')).toEqual('icon.png');
    expect(imgElement.prop('label')).toEqual('Donation');
    expect(imgElement.prop('alt')).toEqual('Donation');
  });
});
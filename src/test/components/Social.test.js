import React from 'react';
import ReactDOM from 'react-dom';
import Social from '../../components/Social.js'

it('<Social /> renders without crashing', () => {
    const div = document.createElement('div');
    const props = {
        video: {
            youtube: ""
        }
    }

    ReactDOM.render(<Social {... props} />, div);
    ReactDOM.unmountComponentAtNode(div);
});
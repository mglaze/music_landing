import React from 'react';
import ReactDOM from 'react-dom';
import Header from '../../components/header/Header.js'

it('<Header /> renders without crashing', () => {
    const div = document.createElement('div');
    const props = {
        albumArt: {
            id: 1,
            albumName: "purpose",
            file: ""
        }
    }
    ReactDOM.render(<Header {...props} />, div);
    ReactDOM.unmountComponentAtNode(div);
});
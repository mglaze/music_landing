import React from 'react';
import { mount } from 'enzyme';
import StreamComponent from '../../components/Streaming';
import { analytics } from "../../utils/analyticsWrapper";

describe('StreamComponent', () => {
  it('should render without crashing', () => {
    const wrapper = mount(
      <StreamComponent
        id="1"
        tag="stream_clicked"
        url="/stream"
        iconImage="icon.png"
        name="Stream"
      />
    );
    expect(wrapper).toBeTruthy();
  });

  it('should call handleClick and change location on click', () => {
    const mockHandleClick = jest.fn();
    const mockAnalyticsEvent = jest.spyOn(analytics, 'event');
    const wrapper = mount(
      <StreamComponent
        id="1"
        tag="stream_clicked"
        url="/stream"
        iconImage="icon.png"
        name="Stream"
      />
    );
    wrapper.instance().handleClick = mockHandleClick;
    wrapper.update();
    wrapper.find('.music-grid-buttons').simulate('click');
    expect(mockHandleClick).toHaveBeenCalledWith('stream_clicked', '');
    console.log('mockAnalyticsEvent calls:', mockAnalyticsEvent.mock.calls);
  });

  it('should render Icons component with correct props', () => {
    const wrapper = mount(
      <StreamComponent
        id="1"
        tag="stream"
        url="/stream"
        iconImage="icon.png"
        name="Stream"
      />
    );
    const iconsComponent = wrapper.find('Icons');
    expect(iconsComponent.prop('label')).toEqual('Stream');
    expect(iconsComponent.prop('iconImage')).toEqual('icon.png');
    expect(iconsComponent.prop('url')).toEqual('/stream');
  });
});

import React from "react";
import SocialView from "../components/views/socialViewComponent";
import release from '../releases/never/data/neverRelease';

const NeverSocial = () => {
    return (
        <div>
            <SocialView release={release} backgroundColor='#1b2e2d' />
        </div>
    );
};
export default NeverSocial;

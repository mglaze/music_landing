import React, { Component } from "react";
import {analytics} from "../../utils/analyticsWrapper";
import {Card} from 'react-bootstrap';


class AlbumCard extends Component {

  handleClick(name,payload) {
    console.log("clickEvent: "+name);
    analytics.event(name,payload);
  }
 ///https://medium.com/@cristi.nord/props-and-how-to-pass-props-to-components-in-react-part-1-b4c257381654

    render() {
        return (
            <div className="social-grid">
                <Card className="card">
                    <div className="img">
                        <Card.Img
                            key={this.props.id}
                            src={this.props.src}
                            alt={this.props.name}
                            className="img-card"
                            as="img"
                            variant="top"
                            onClick={() => this.handleClick(this.props.tag, "")}
                        />
                    </div>
                </Card>
            </div>
        );
  }
}

export default AlbumCard;